<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('states', function(Blueprint $table){
            $table->increments("id");
            $table->string("code",5)->unique();
            $table->string("region_code");
            $table->string("name")->unique();
            $table->string("short_code",3)->unique();
            $table->string("size");
            $table->timestamps();
            $table->foreign('region_code')->references('code')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('states');
    }
}
