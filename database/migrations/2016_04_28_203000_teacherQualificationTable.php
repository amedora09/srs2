<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeacherQualificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("qualifications",function(Blueprint $table){
            $table->increments("id");
            $table->integer("teacher_id")->unsigned();
            $table->string("institute_name");
            $table->string("level");
            $table->string("qualification");
            $table->string("subject");
            $table->string("type");
            $table->string("grade");
            $table->string("cert_date");
            $table->timestamps();
            $table->foreign("teacher_id")->references("id")->on("teachers");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
