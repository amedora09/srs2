<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("students",function(Blueprint $table){
            $table->increments("id");
            $table->string("school_code");
            $table->string("admiss_no");
            $table->string("first_name");
            $table->string("last_name");
            $table->string("other_name");
            $table->string("dob");
            $table->string("sex");
            $table->string("srs_code");
            $table->decimal("score_point",3,2);
            $table->timestamps();
            $table->foreign("school_code")->references("code")->on("schools");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("students");
    }
}
