<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LgasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('lgas',function(Blueprint $table){
            $table->increments("id");
            $table->string("code")->unique();
            $table->integer("state_id")->unsigned();
            $table->string("name")->unique();
            $table->string("short_name",5)->unique();
            $table->timestamps();
            $table->foreign('state_id')->references('id')->on('states');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("lgas");
    }
}
