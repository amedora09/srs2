<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("schools",function(Blueprint $table){
            $table->increments("id");
            $table->string("code");
            $table->integer("lga_id")->unsigned();
            $table->string("name")->unique();
            $table->string("address");
            $table->decimal("non_achievable_risk",3,2);
            $table->timestamps();
            $table->foreign('lga_id')->references('id')->on('lgas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("schools");
    }
}
