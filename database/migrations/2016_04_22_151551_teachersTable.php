<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("teachers",function(Blueprint $table){
            $table->increments("id");
            $table->integer("school_id")->unsigned();
            $table->string("first_name");
            $table->string("last_name");
            $table->string("other_name");
            $table->string("dob");
            $table->string("sex");
            $table->string("srs_code");
            $table->decimal("score_point",3,2);
            $table->integer("subject_id")->unsigned();
            $table->timestamps();
            $table->foreign("school_id")->references("id")->on("schools");
            $table->foreign("subject_id")->references("id")->on("subjects");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("teachers");
    }
}
