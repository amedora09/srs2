<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("achievements",function(Blueprint $table){
            $table->increments("id");
            $table->string("srs_code");
            $table->string("subject_code");
            $table->integer("level_id");
            $table->string("lga_code");
            $table->string("state_code");
            $table->string("school_code");
            $table->string("region_code");
            $table->string("session");
            $table->string("term");
            $table->string("class");
            $table->decimal("score",5,2);
            $table->decimal("progression",5,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
