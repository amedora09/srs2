<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentsAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /* Schema::create("attendances",function(Blueprint $table){
             $table->increments("id");
             $table->string("srs_code");
             $table->string("subject_code");
             $table->integer("level_id")->unsigned();
             $table->string("lga_code");
             $table->string("state_code");
             $table->string("school_code");
             $table->string("region_code");
             $table->string("session");
             $table->string("term");
             $table->string("class");
             $table->decimal("score",5,2);
             $table->timestamps();
             $table->foreign("srs_code")->references("srs_code")->on("students");
             $table->foreign("subject_code")->references("code")->on("subjects");
             $table->foreign("state_code")->references("code")->on("states");
             $table->foreign("school_code")->references("code")->on("schools");
             $table->foreign("level_id")->references("id")->on("levels");
             $table->foreign("region_code")->references("code")->on("regions");
             $table->foreign("lga_code")->references("code")->on("lgas");

         });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("attendances");
    }
}
