<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeachingsTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
       /* Schema::create("teachings",function(Blueprint $table){
            $table->increments("id");
            $table->integer("teacher_id")->unsigned();
            $table->string("subject_code");
            $table->string("school_code");
            $table->string("commission");
            $table->string("relevance");
            $table->timestamps();
            $table->foreign("teacher_id")->references("id")->on("teachers");
            $table->foreign("subject_code")->references("code")->on("subjects");
            $table->foreign("school_code")->references("code")->on("schools");
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("teachings");
    }
}
