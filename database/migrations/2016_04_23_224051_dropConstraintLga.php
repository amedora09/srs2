<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropConstraintLga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('lgas', function ($table) {
            //$table->dropForeign('lgas_state_id_foreign');
            $table->dropUnique('lgas_name_unique');
           // $table->foreign('state_id')->references('code')->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
