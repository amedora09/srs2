<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("facilities",function(Blueprint $table){
            $table->increments("id");
            $table->string("lga_code");
            $table->string("school_code");
            $table->float("library",5,2);
            $table->float("furniture",5,2);
            $table->float("building",5,2);
            $table->string("session");
            $table->timestamps();
            $table->foreign("lga_code")->references("code")->on("lgas");
            $table->foreign("school_code")->references("code")->on("schools");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
