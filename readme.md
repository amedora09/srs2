# BaseAPI - API Framework in Phalcon

## Development on your local machine

### Set your Application Environment
```
#!apache

SetEnv APPLICATION_ENV "local"
```

### Set DB credentials for Your API
```
#!apache
SetEnv DB_PASSWORD "INSERT_PASSWORD"
```

## Staging Server

### Set your Application Environment
```
#!apache
SetEnv APPLICATION_ENV "staging"
```

### Set DB credentials for Your API
```
#!apache
SetEnv DB_PASSWORD "INSERT_PASSWORD"
```

## Production

### Set your Application Environment
```
#!apache
SetEnv APPLICATION_ENV "production"
```

### Set DB credentials for Your API
```
#!apache
SetEnv DB_PASSWORD "INSERT_PASSWORD"
```