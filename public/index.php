<?php
require dirname(__DIR__) . '/vendor/autoload.php';

use Phalcon\Mvc\Micro\Collection as MicroCollection;

error_reporting(E_ALL);

date_default_timezone_set('UTC');

try {
    /**
     * Read the configuration
     */

    $env = getenv('APPLICATION_ENV');

    switch ($env) {
        case 'production':
            error_reporting(0);
            $config = include __DIR__ . '/../app/config/config_prod.php';
            break;

        case 'staging':
            $config = include __DIR__ . '/../app/config/config_staging.php';
            break;

        case 'development':
        case 'local':
        default:
            ini_set('display_errors', 1);
            $config = include __DIR__ . '/../app/config/config_local.php';
            break;
    }
    /**
     * Read auto-loader
     */
    include __DIR__ . '/../app/config/loader.php';

    /**
     * Read services
     */
    include __DIR__ . '/../app/config/services.php';

    /**
     * Create Application
     */
    $app = new \Phalcon\Mvc\Micro($di);

    /**
     * Add your routes here
     */

    /**
     * Expose the /v1/states end point
     */
    $state = new MicroCollection();
    $state->setHandler('StateController', true);
    $state->setPrefix('/v1/states');
    $state->get('/', 'getStates');
    $state->get('/lgas/{state_code}', 'getLgas');
    $app->mount($state);

    /**
     * Exposes thi /v1/
     */

    /**
     * Not found handler
     */
    $app->notFound(function () use ($app) {
        $app->response->setStatusCode(404, HttpStatusCodes::getMessage(404))->sendHeaders();
        $app->response->setContentType('application/json');
        $app->response->setJsonContent([
            'status'  => 'error',
            'message' => ResponseMessages::METHOD_NOT_IMPLEMENTED,
            'code'    => ResponseCodes::METHOD_NOT_IMPLEMENTED
        ]);
        $app->response->send();
    });

    $app->handle();
} catch (\Exception $e) {
    $app->response->setStatusCode(500, HttpStatusCodes::getMessage(500))->sendHeaders();

    echo json_encode([
        'status'  => 'error',
        'message' => ResponseMessages::INTERNAL_SERVER_ERROR,
        'code'    => ResponseCodes::INTERNAL_SERVER_ERROR,
        'ex'      => $e->getMessage()
    ]);
};
