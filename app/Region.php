<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/28/16
 * Time: 7:49 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Region extends Model {

    function states(){
        return $this->hasMany("App\State","region_code","code");
    }
} 