<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/28/16
 * Time: 6:40 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class State extends Model {

    public function region(){
        return $this->belongsTo("App\State");
    }

    public function lgas(){
        return $this->hasMany("App\Lga","state_code","code");
    }

    public function facilities(){
        return $this->hasManyThrough("App\Facility","App\Lga","state_code","lga_code","code");
    }
} 