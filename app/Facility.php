<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/29/16
 * Time: 2:09 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Facility extends Model {
    /*public function state(){                 */
    /*    return $this->belongsTo("App\State");*/
    /*}                                        */

    public function lga(){
        return $this->belongsTo("App\Lga");
    }
} 