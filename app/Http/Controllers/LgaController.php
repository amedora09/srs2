<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/23/16
 * Time: 8:52 PM
 */

namespace App\Http\Controllers;
/*use Laravel\Lumen\Routing\Controller;*/

use App\Lga;

class LgaController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getList(){
        $result=[];
        $lgas =\App\Lga::all();
        if(count($lgas)>0){
            $result['success']  =true;
            $result['data']     =$lgas;
            $result['msg']      ="Query Valid";
            $result['code']     ="200";
        }else{
            $result['success']  =false;
            $result['data']     =$lgas;
            $result['msg']      ="Invalid query";
            $result['code']     ="401";
        }
        return response()->json($result);
    }

    public function loadExcel(){
        $destinationPath =public_path()."/";
        $fileName = "listoflgas1.xls";
        $destinationPath = $destinationPath.$fileName;

        // $request->file('filexlx')->move(public_path()."/documents/","drivers.xls");
        try{

            $objPHPExcel = \PHPExcel_IOFactory::load($destinationPath);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            //$highestRow = $sheetData->getHighestRow(); // e.g. 10
            //$highestColumn = $sheetData->getHighestColumn(); // e.g 'F'
            $z=1;
            foreach($sheetData as $data){

                if($z !=1){ //Skips the sheet headings

                    if(!empty($data["A"])){

                        // if(DB::table("drivers")->max("id") != null){


                        //$mail= DB::table("drivers")->where('email', '=', strtolower($license) ."@gmail.com")->get();
                        $ph = DB::table("lgas")->where('state_id', '=', $data['A'])->get();
                        // dd($ph);
                        if(count($ph) == 0 ){
                            $lga = new Lga();
                            //$d = DB::table("drivers")->where("phone",$data['D'])->first()
                            $lga->code ="";
                            $lga->state_id = $data['B'];
                            $lga->name  =   $data['A'];
                            $lga->created_at            =   date("Y-m-d H:i:s");
                            $lga->save();

                            echo $lga->id ."<br/>";
                        }else{

                        }

                    }
                }
                $z++;
            }



        }catch(Exception $ex ){
            echo $ex->getMessage();
        }

    }


} 