<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/24/16
 * Time: 5:27 PM
 */

namespace App\Http\Controllers;

use App\School;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class SchoolController extends Controller {

    public function __construct()
    {
        //
    }

    public function getList(){
        $result=[];
        $sch =\App\School::all();
        if(count($sch)>0){
            $result['success']  =true;
            $result['data']     =$sch;
            $result['msg']      ="Data available";
            $result['code']     ="200";
        }else{
            $result['success']  =false;
            $result['data']     =$sch;
            $result['msg']      ="Unexpected Error";
            $result['code']     ="401";
        }
        return response()->json($result);
    }


    public function schoolData(Request $request,$code){
        $result=[];
        $sch =\App\School::where("code",$code)->first();
        if(count($sch)>0){
            $result['success']  =true;
            $result['data']     =$sch;
            $result['msg']      ="Data available";
            $result['code']     ="200";
        }else{
            $result['success']  =false;
            $result['data']     =$sch;
            $result['msg']      ="Unexpected Error";
            $result['code']     ="401";
        }
        return response()->json($result);
    }


    public function LoadExcel(){
        $destinationPath ="";
        $fileName = "kadunasouthlgaschools.xls";
        $destinationPath = $destinationPath.$fileName;

        // $request->file('filexlx')->move(public_path()."/documents/","drivers.xls");
        try{

            $objPHPExcel = \PHPExcel_IOFactory::load($destinationPath);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            //$highestRow = $sheetData->getHighestRow(); // e.g. 10
            //$highestColumn = $sheetData->getHighestColumn(); // e.g 'F'
            $z=1;
            foreach($sheetData as $data){

                if($z !=1){ //Skips the sheet headings

                    if(!empty($data["A"])){

                        // if(DB::table("drivers")->max("id") != null){


                        //$mail= DB::table("drivers")->where('email', '=', strtolower($license) ."@gmail.com")->get();
                        //$ph = DB::table("schools")->where('state_id', '=', $data['B'])->get();
                        $l =DB::table("schools")->max("id");
                        $code = str_pad($l,6,"0",STR_PAD_LEFT); //str_pad($l, 3,0);

                        // dd($ph);
                        //  if(count($ph) == 0 ){
                        $sch = new School();
                        //$d = DB::table("drivers")->where("phone",$data['D'])->first()
                        $sch->code =$code;
                        $sch->lga_id = $data['C'];
                        $sch->address = $data['B'];
                        $sch->name  =   $data['A'];
                        $sch->created_at            =   date("Y-m-d H:i:s");
                        $sch->save();

                        echo $sch->id ."<br/>";
                        // }else{

                        // }

                    }
                }
                $z++;
            }
        }catch(Exception $ex ){
            echo $ex->getMessage();
        }
    }

} 