<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/27/16
 * Time: 11:33 PM
 */

namespace App\Http\Controllers;


use App\Achievement;
use App\Level;
use App\Lga;
use App\School;
use App\State;
use App\Student;

class AchievementController extends Controller {
    public function __construct(){

    }

    public function LoadExcel(){
        $destinationPath =""; // File destination in public dir
        $fileName = "juniorschool1201320142ndterm.xls"; //file
        $destinationPath = $destinationPath.$fileName;
        try{

            $objPHPExcel = \PHPExcel_IOFactory::load($destinationPath);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);



            //$objReader = new PHPExcel_Reader_Excel2007();
            //$objReader->setReadDataOnly(true);
            //$objPHPExcel = $objReader->load($file);


            $excelEnglishCol = $sheetData[5]["D"]; // fIELD FOR ENGLISH
            $excelMathsCol   = $sheetData[5]['E']; //fIELD FOR MATHEMATICS
            $excelBiology    = $sheetData[5]['F']; //
            $excelChemCol    = $sheetData[5]['G'];
            $excelPhysicCol	 = $sheetData[5]['H'];
            $excelCiviCol    = $sheetData[5]['I'];
            $excelBT         = $sheetData[5]['J'];
            $excelSocial     = $sheetData[5]['K'];
            $excelBus        = $sheetData[5]['L'];
            $excelPhe        = $sheetData[5]['M'];
            $excelHome       = $sheetData[5]['N'];
            $excelAgric      = $sheetData[5]['O'];
            $excelCrs         = $sheetData[5]['P'];
            $excelIrs        = $sheetData[5]['Q'];
            $excelYor        = $sheetData[5]['R'];
            $excelIgb        = $sheetData[5]['S'];
            $excelfrench        = $sheetData[5]['T'];
            $excelIt      = $sheetData[5]['U'];
            $excelMusic        = $sheetData[5]['V'];
            $excelArt        = $sheetData[5]['W'];
            $excelDiction        = $sheetData[5]['X'];
            $excelCulture        = $sheetData[5]['Y'];

            $school_code = $sheetData[1]['A'];
            $session = $sheetData[2]['A'];
            $levelData = explode("-",trim($sheetData[3]['A']));
            $level = Level::where("name",strtolower(trim($levelData[0])))->pluck("id");
            $class = $levelData[1];
            $term = $sheetData[4]['A'];
            $school = School::where('code',trim($school_code))->first();
            $lga = Lga::where("code",trim($school->lga_code))->first();
            $state = State::where("code",$lga->state_id)->first();


            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
            {
                $worksheetTitle = $worksheet->getTitle();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);

                // expects same number of row records for all columns
                $highestRow = $worksheet->getHighestRow();
                for ($row = 6; $row <= $highestRow-1; $row++)
                {
                    for($col = 0; $col < $highestColumnIndex; $col++)
                    {

                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                       // echo $val ;
                        $student = Student::where('admiss_no',trim($sheetData[$row]['B']))->first();
                        if($student && !empty($val)){
                            $progress = new Achievement();
                            $progress->srs_code = $student->srs_code;
                            $progress->school_code = $student->school_code;

                            //Get State Info;

                            $progress->lga_code = $lga->code;
                            $progress->state_code = $state->code;
                            $progress->region_code = $state->region_code;
                            $progress->session = $session;
                            $progress->class = $class;
                            $progress->term = $term;
                            $progress->level_id = $level[0];
                            $colIndex =$cell->getColumn();// gets the column index like A,B,C \PHPExcel_Cell::columnIndexFromString($cell->getColumn());

                            if($colIndex == "D"){
                                $progress->subject_code = '001';
                                $progress->subject = $excelEnglishCol;
                                $progress->score= $val;
                                 // gets the level ID
                                $progress->save();
                            }elseif($colIndex == "E"){
                                $progress->subject_code = '005';
                                $progress->subject = $excelMathsCol;
                                $progress->score= $val;
                                // gets the level ID
                                $progress->save();
                            }elseif($colIndex == "I"){
                                $progress->subject_code = '009';
                                $progress->subject = $excelCiviCol;
                                $progress->score= $val;
                                // gets the level ID
                                $progress->save();
                            }elseif($colIndex == "J"){
                                if(strtolower($level) == "lower basic" ){
                                    $progress->subject_code = '006';
                                    $progress->subject = "Basic Science";
                                    $progress->score= $val;
                                }else{
                                    $progress->subject_code = '007';
                                    $progress->subject = $excelBT;
                                    $progress->score= $val;
                                }
                                // gets the level ID
                                $progress->save();
                            }elseif($colIndex == "K"){
                                $progress->subject_code = '008';
                                $progress->subject = $excelSocial;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "L"){
                                $progress->subject_code = '019';
                                $progress->subject = $excelBus;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "M"){
                                $progress->subject_code = '013';
                                $progress->subject = $excelPhe;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "N"){
                                $progress->subject_code = '017';
                                $progress->subject = $excelHome;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "O"){
                                $progress->subject_code = '016';
                                $progress->subject = $excelAgric;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "P"){
                                $progress->subject_code = '011';
                                $progress->subject = $excelCrs;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "Q"){
                                $progress->subject_code = '012';
                                $progress->subject = $excelCrs;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "R"){
                                $progress->subject_code = '002';
                                $progress->subject = $excelYor;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "S"){
                                $progress->subject_code = '003';
                                $progress->subject = $excelIgb;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "U"){
                                $progress->subject_code = '015';
                                $progress->subject = $excelIt;
                                $progress->score= $val;

                                $progress->save();
                            }elseif($colIndex == "W"){
                                $progress->subject_code = '010';
                                $progress->subject = $excelArt;
                                $progress->score= $val;

                                $progress->save();
                            }



                            /**Term to add result per term */
                            $progress->term = $term;
                            //$progress->score =


                        }

                    }
                    //echo $session."<br><br>".$term."<br><br>";
                }
            }





        }catch(Exception $ex ){
            echo $ex->getMessage();
        }
    }
}