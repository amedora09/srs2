<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/27/16
 * Time: 3:00 PM
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Subject;
class SubjectController extends Controller{

    public function __construct()
    {
        //
    }

    public function LoadExcel(){
        $destinationPath =""; // File destination in public dir
        $fileName = "subjects.xls"; //file
        $destinationPath = $destinationPath.$fileName;
        try{

            $objPHPExcel = \PHPExcel_IOFactory::load($destinationPath);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

            $z=1;
            //loop through datasheet by row
            foreach($sheetData as $data){

                if($z !=1){ //Skips the sheet headings

                    if(!empty($data["A"])){
                        $l =DB::table("subjects")->max("id");
                        $code = str_pad($l,3,"0",STR_PAD_LEFT); //str_pad($l, 3,0);
                        $sub = new Subject();
                        $sub->code =$code;
                        $sub->level = $data['B'];
                        $sub->description="";
                        $sub->name  =   $data['A'];
                        $sub->type = $data['C'];
                        $sub->updated_at = date("Y-m-d H:i:s");
                        $sub->created_at            =   date("Y-m-d H:i:s");
                        $sub->save();

                        echo $sub->id ."<br/>";
                    }
                }
                $z++;
            }
        }catch(Exception $ex ){
            echo $ex->getMessage();
        }
    }
} 