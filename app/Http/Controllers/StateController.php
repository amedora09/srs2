<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/29/16
 * Time: 12:53 AM
 */

namespace App\Http\Controllers;


use App\Facility;
use App\Lga;
use App\State;
use Illuminate\Support\Facades\DB;

class StateController extends Controller {
/*Get list of states*/
    public function getList(){
        $result=[];
        $state =\App\State::all();
        if(count($state)>0){
            $result['success']  =true;
            $result['data']     =$state;
            $result['msg']      ="Data Available";
            $result['code']     ="200";
        }else{
            $result['success']  =false;
            $result['data']     =$state;
            $result['msg']      ="Invalid Query";
            $result['code']     ="401";
        }
        return response()->json($result);
    }

    /** Get list of local governmet by  state*/
    public function getLgaList($code){
        $result=[];

        $lgas =State::where("code",$code)->first()->lgas;
        if(count($lgas)>0){
            $result['success']  =true;
            $result['data']     =$lgas;
            $result['msg']      ="Data Available";
            $result['code']     ="200";
        }else{
            $result['success']  =false;
            $result['data']     =$lgas;
            $result['msg']      ="Invalid Query";
            $result['code']     ="401";
        }
        return response()->json($result);
    }

    /*Section to return State home page*/

    public function getStateIndexData($code){

        /**
         * Section to get
         * Furniture and Library total
         */
        $dbLibrary = DB::table("facilities")
            ->join('lgas', 'lgas.code', '=', 'facilities.lga_code')
            ->join('states', 'states.code', '=', 'lgas.state_code')
            ->where('states.code', '=', $code)
            ->get();
        $libraryTotal = 0;
        $furnitureTotal =0;
        $x=1;
        //Get sum of total library facility nation wide
        $nationalTotalLibrary = Facility::where("id",">",0)->sum("library");
        // Get sum of totAL furniture facility nation wide
        $nationalTotalFurniture = Facility::where("id",">",0)->sum("furniture");

       // dd($nationalTotalLibrary);
        foreach($dbLibrary as $facility){
            $libraryTotal +=$facility->library;
            $furnitureTotal += $facility->furniture;
         $x++;
        }



        $libraryPercentage = $libraryTotal/$nationalTotalLibrary * 100; // percentage library facility
        $furniturePercentage = $furnitureTotal/$nationalTotalFurniture * 100; //percentage furniture facility

        /**
         * End of section for facility state data
         */

        //Dummy student Attendance
        $studentAttendance =$libraryTotal/$nationalTotalLibrary * 95;
        //Dummy Student Assessment
        $studentAssessment = $furnitureTotal/$nationalTotalFurniture * 95;
        //Dummy Qualification relevance
        $qualificationRelevance = $furnitureTotal/$nationalTotalFurniture * 150;
        $TeachersTraining = $furnitureTotal/$nationalTotalFurniture * 110;

//->select(DB::raw("(SELECT code, students.school_code, count(students.id) as stdntcount) as v"))
        $dbStudentsAttendance = DB::table("students")
            ->join('schools', 'schools.code', '=', 'students.school_code')
            ->leftJoin('lgas',"lgas.code","=","schools.lga_code")
            ->leftJoin('states', 'states.code', '=', 'lgas.state_code')->select(DB::raw('IFNULL(COUNT(students.id), 0) as cnt'),'lgas.code','lgas.name',
            'schools.non_achievable_risk','lgas.risk_factor')
            ->where('states.code', '=', $code)->groupBy("lgas.code")
            ->get();



        //Students by local governments in state

       // $students =

        if(count($dbLibrary)>0){

            $result['success']               = true;
            $result['data']['library']       = $libraryPercentage;
            $result['data']['furniture']     = $furniturePercentage;
            $result['data']['assessment']    = $studentAssessment;
            $result['data']['attendance']    = $studentAttendance;
            $result['data']['relevance']     = $qualificationRelevance;
            $result['data']['training']      = $TeachersTraining;
            $result['data']['attendanceTable'] = $dbStudentsAttendance;
            $result['msg']      ="Data Available";
            $result['code']     ="200";
        }else{
            $result['success']  =false;
            $result['data']     =null;
            $result['msg']      ="Invalid Query";
            $result['code']     ="401";
        }
        return response()->json($result);
    }

    public function getStateIndexData2($code){
        //$totalStudentAtRisk =
    }
} 