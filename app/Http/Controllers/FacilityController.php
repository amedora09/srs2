<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/29/16
 * Time: 3:09 AM
 */

namespace App\Http\Controllers;


use App\Facility;
use Illuminate\Support\Facades\DB;
class FacilityController extends Controller {

    public function loadExcel(){
        $destinationPath ="";
        $fileName = "schools.xls";
        $destinationPath = $destinationPath.$fileName;

        // $request->file('filexlx')->move(public_path()."/documents/","drivers.xls");
        try{

            $objPHPExcel = \PHPExcel_IOFactory::load($destinationPath);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            //$highestRow = $sheetData->getHighestRow(); // e.g. 10
            //$highestColumn = $sheetData->getHighestColumn(); // e.g 'F'
            $z=1;
            foreach($sheetData as $data){
                if(true){ //Skips the sheet headings
                    if(!empty($data["A"])){
                        //$mail= DB::table("drivers")->where('email', '=', strtolower($license) ."@gmail.com")->get();
                        //$ph = DB::table("lgas")->where('state_id', '=', $data['A'])->get();
                        // dd($ph);
                       //if(count($ph) == 0 ){
                            $facility  = new Facility();
                            //$d = DB::table("drivers")->where("phone",$data['D'])->first()

                            $facility->lga_code         = $data['B'];
                            $facility->school_code      =   $data['A'];
                            $facility->library          = rand(1,100);
                            $facility->furniture        = rand(10,100);
                            $facility->building         =rand(10,100);
                            $facility->session          = "2015/2016";
                            $facility->created_at            =   date("Y-m-d H:i:s");
                            $facility->save();
                            echo $facility->id ."<br/>";
                    }
                }
                $z++;
            }
        }catch(Exception $ex ){
            echo $ex->getMessage();
        }

    }

} 