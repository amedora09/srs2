<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/27/16
 * Time: 9:26 PM
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Student;

class StudentController extends Controller {
    public function __construct(){

    }

    public function LoadExcel(){
        $destinationPath ="";
        $fileName = "chrislandjunior1.xls";
        $destinationPath = $destinationPath.$fileName;

        // $request->file('filexlx')->move(public_path()."/documents/","drivers.xls");
        try{

            $objPHPExcel = \PHPExcel_IOFactory::load($destinationPath);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            //$highestRow = $sheetData->getHighestRow(); // e.g. 10
            //$highestColumn = $sheetData->getHighestColumn(); // e.g 'F'
            $z=1;
            foreach($sheetData as $data){
                if($z !=1){ //Skips the sheet headings

                    if(!empty($data["A"])){

                        // if(DB::table("drivers")->max("id") != null){


                        //$mail= DB::table("drivers")->where('email', '=', strtolower($license) ."@gmail.com")->get();
                        //$ph = DB::table("schools")->where('state_id', '=', $data['B'])->get();
                        $l =DB::table("students")->max("id");
                        $code = str_pad($l,9,"0",STR_PAD_LEFT); //str_pad($l, 3,0);

                        // dd($ph);
                        //  if(count($ph) == 0 ){
                        $std = new Student();
                        //$d = DB::table("drivers")->where("phone",$data['D'])->first()
                        $std->srs_code =$code;
                        /** Get the column for name $data[A]
                         * split on blank space
                         */
                        $nameData = explode(" ",$data['A']);
                        if(is_array($nameData) && count($nameData) >1){
                            $std->last_name = $nameData[0];
                            $std->first_name = $nameData[1];
                        }else{
                            $std->last_name = $nameData['A'];
                            $std->first_name = "";
                        }



                        $std->other_name="";
                        $std->dob ="";
                        $std->sex ="";

                        $std->school_code = $data['B'];

                        $std->created_at            =   date("Y-m-d H:i:s");
                        $std->save();

                        echo $std->id ."<br/>";
                        // }else{

                        // }

                    }
                }
                $z++;
            }
        }catch(Exception $ex ){
            echo $ex->getMessage();
        }
    }
} 