<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/28/16
 * Time: 7:50 PM
 */

namespace App\Http\Controllers;


use App\Region;

class RegionController extends Controller{
    public function __construct(){

    }

    public function getList(){

        $result=[];
        $regions =Region::all();
        if(count($regions)>0){
            $result['success']  =true;
            $result['data']     =$regions;
            $result['msg']      ="Data Updated";
            $result['code']     ="200";
        }else{
            $result['success']  =false;
            $result['data']     =$regions;
            $result['msg']      ="Invalid Query";
            $result['code']     ="401";
        }
        return response()->json($result);
    }

    public function getRegionStates($id){
        return Region::where("code",$id)->first()->states;
    }
} 