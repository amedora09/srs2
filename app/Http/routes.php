<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//use \Illuminate\Support\Facades\Request;


use \Illuminate\Http\Request;
use App\Lga;
$app->get('/', function () use ($app) {
    return $app->version();
});


$app->get('/index', function () use ($app) {
    return $app->version();
});

$app->get('loadlga', [
    'as' => 'profile', 'uses' => 'LgaController@loadExcel'
]);

$app->get('loadSchoolExcel',['as'=>'epeschools','uses'=>'SchoolController@LoadExcel']);
$app->get('loadSubjectExcel',['as'=>'subjects','uses'=>'SubjectController@LoadExcel']);
$app->get('loadStudentExcel',['as'=>'studentName','uses'=>'StudentController@LoadExcel']);
$app->get('loadProgressExcel',['as'=>'progress','uses'=>'AchievementController@LoadExcel']);
$app->get('loadFacilityExcel',["uses"=>"FacilityController@LoadExcel"]);

$app->get('excel/loadlga', function(){
    $destinationPath ="";
    $fileName = "listoflgas1.xls";
    $destinationPath = $destinationPath.$fileName;

    // $request->file('filexlx')->move(public_path()."/documents/","drivers.xls");
    try{

        $objPHPExcel = \PHPExcel_IOFactory::load($destinationPath);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        //$highestRow = $sheetData->getHighestRow(); // e.g. 10
        //$highestColumn = $sheetData->getHighestColumn(); // e.g 'F'
        $z=1;
        foreach($sheetData as $data){

            if($z !=1){ //Skips the sheet headings

                if(!empty($data["A"])){
                    // if(DB::table("drivers")->max("id") != null){
                    // $mail= DB::table("drivers")->where('email', '=', strtolower($license) ."@gmail.com")->get();
                    $ph = DB::table("lgas")->where('state_id', '=', $data['B'])->get();
                    $l  = DB::table("lgas")->max("id");
                    $code = str_pad($l,4,"0",STR_PAD_LEFT); //str_pad($l, 3,0);
                    // dd($ph);
                  //  if(count($ph) == 0 ){
                        $lga = new Lga();
                        //$d = DB::table("drivers")->where("phone",$data['D'])->first()
                        $lga->code              = $code;
                        $lga->state_id          = $data['B'];
                        $lga->name              = $data['A'];
                        $lga->created_at        = date("Y-m-d H:i:s");
                        $lga->save();
                        echo $lga->id ."<br/>";
                   // }else{
                   // }
                }
            }
            $z++;
        }
    }catch(Exception $ex ){
        echo $ex->getMessage();
    }
});

//Pull to get drop down of regions
$app->get('/region/list',['uses'=>"RegionController@getList"]);
//gets List of state in a region
$app->get('/region/data/{code}',['uses'=>"RegionController@getRegionStates"]);

$app->get('/state/list',['uses'=>'StateController@getList']);
//gets List of state in a region
$app->get('/state/lgalist/{code}',['uses'=>"StateController@getLgaList"]);
$app->get('/state/index/{code}',['uses'=>"StateController@getStateIndexData"]);

//Local Government List
$app->get('/lga/list',['uses'=>"LgaController@getList"]);
$app->get('/school/list',['uses'=>"SchoolController@getList"]);
//School detail by ID
$app->get('/school/data/{code}', ['uses'=>'SchoolController@schoolData']);
?>


