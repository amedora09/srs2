<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 4/23/16
 * Time: 8:54 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Lga extends Model {

    public function state(){
        return $this->belongsTo("App\State");
    }

    public function facilities(){
        return $this->hasMany("App\Facility","lga_code","code");
    }
} 